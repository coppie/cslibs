C Single-Header Libraries
=========================
A collection of single-header libraries written in C.


Usage
-----
Copy library header into your project and include it according to:
[how to use stb-style libraries][1].
Library dependant implementation macro can be found near the beginning of each
library header and is of form `LIBRARYNAME_IMPLEMENTATION`.

[1]: https://github.com/nothings/stb#how-do-i-use-these-libraries "stb-style how to"


License
-------
This project is dual-licensed under public domain and The MIT License.
See LICENSE for more information.
